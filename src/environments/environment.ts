// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDBa-bZIka6gJOX3HjrLsaMwo8zXlm6vTY",
    authDomain: "routine-15c53.firebaseapp.com",
    databaseURL: "https://routine-15c53.firebaseio.com",
    projectId: "routine-15c53",
    storageBucket: "routine-15c53.appspot.com",
    messagingSenderId: "140912583378",
    appId: "1:140912583378:web:96e5515b836c9054c5c30b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
