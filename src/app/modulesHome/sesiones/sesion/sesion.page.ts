import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from "@ionic/angular";
import { Router, ActivatedRoute } from '@angular/router';
import { DatabaseService } from "../../../services/database.service";
import { Session } from '../../../model/session.module';


@Component({
  selector: 'app-sesion',
  templateUrl: './sesion.page.html',
  styleUrls: ['./sesion.page.scss'],
})
export class SesionPage implements OnInit {

  data: any;
  idSession:string;
  ejercicios: any[] = []
  repes: any[] = []
  peso: any[] = []
  session: Session[] = []


  constructor(private alertController: AlertController, public dbService: DatabaseService, private router: Router, private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
    if(this.activatedRoute.snapshot.paramMap.get("id") != null){
      this.idSession = this.activatedRoute.snapshot.paramMap.get("id");
    }
    
    this.dbService.getSession(this.idSession).subscribe(res => {
      this.data = res;
      this.ejercicios = this.data.ejercicios;
      this.repes = this.data.repeticiones;
      this.peso = this.data.peso;

      let cont = 0;
      let s: Session;
      this.ejercicios.forEach(item =>{
        s = {
          ejercicio: item,
          repes: this.repes[cont],
          peso: this.peso[cont]
        }
        this.session.push(s);
        cont++
      })
    })

  }

  async presentAlert(header, message) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Vale']
    });

    await alert.present();
  }

  async presentAlertSub(header, subHeader, message) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: ['Vale']
    });

    await alert.present();
    return true;
  }

}
