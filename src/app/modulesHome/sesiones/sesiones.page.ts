import { Component, OnInit } from '@angular/core';
import { DatabaseService } from "../../services/database.service";
import { Sessiones } from "../../model/sessiones.module";
import { ToastController } from '@ionic/angular';
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-sesiones',
  templateUrl: './sesiones.page.html',
  styleUrls: ['./sesiones.page.scss'],
})
export class SesionesPage implements OnInit {

  sesiones: Sessiones[] = [];
  data: any[] = [];
  weekday = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves" , "Viernes", "Sabado"];

  constructor(private alertController: AlertController, private dbService : DatabaseService, public toastController: ToastController) { }

  ngOnInit() {
    this.dbService.getSesions().subscribe( res => {
      //Guardo los datos de la bd en un array.
      this.data = [];
      this.sesiones = [];
      this.data = res;
     
      //Guardo sesion
      var aux:Sessiones;
      this.data.forEach(item => {
        aux = {
          dia: "",
          date: item.date,
          ejercicios: item.ejercicios,
          peso: item.peso,
          repes: item.repes
        }
        var dia = new Date(aux.date)
        aux.dia = this.weekday[dia.getDay()];
        this.sesiones.push(aux);
      })
    });
  }

  async presentAlertDelete(header, message, session_id) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Vale',
          handler: () => {
            this.deleteSesion(session_id);
          }
        }
      ]
    });

    await alert.present();
    return true;
  }

  deleteSesion(session_id){
    this.dbService.deleteSesion(session_id).then(res =>{
      this.presentToast("Sesion eliminada correctamente");
    }).catch(err =>{
      this.presentToast("Error al eliminar una sesion");
    })
  };

  async presentToast(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

}
