import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { User } from "../../model/user.module";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  readOnly = true;
  data: any;
  nombre:string
  peso:string
  objetivo:string
  user:User

  constructor(private dbService: DatabaseService) { }

  ngOnInit() {
    this.dbService.getPerfil("javier").subscribe( res => {
      this.data = res;
      this.nombre = this.data.nombre,
      this.peso= this.data.peso,
      this.objetivo= this.data.objetivo
      this.user = {
        nombre: this.nombre,
        peso: this.peso,
        objetivo: this.objetivo
      }
    });
  }

  editPerfil(){
    this.readOnly = false;
    this.ngOnInit();
  }

  savePerfil(){
    this.readOnly = true;
    this.saveData()
    this.ngOnInit();
  }

  saveData(){
    this.user = {
      nombre: this.nombre,
      peso: this.peso,
      objetivo: this.objetivo
    }
    this.dbService.savePerfil(this.user, "javier");
  }

}
