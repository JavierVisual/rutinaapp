import { Component, OnInit } from '@angular/core';
import { Ejercicio } from "../model/ejercicio.module";
import { AlertController } from "@ionic/angular";
import { Router, ActivatedRoute } from '@angular/router';
import { DatabaseService } from "../services/database.service";
import { Sessiones } from '../model/sessiones.module';
import { ModalController } from '@ionic/angular';
import { SesionesPage } from '../modulesHome/sesiones/sesiones.page';

@Component({
  selector: 'app-work',
  templateUrl: './work.page.html',
  styleUrls: ['./work.page.scss'],
})
export class WorkPage{

  ejercicios: Ejercicio[] = [ ];
  data: any[] = [];
  weekday = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves" , "Viernes", "Sabado"];
  idDay;
  diaSem:string;
  completed = false;
  fecha: string;
  date:Date;
  save:boolean = false;

  constructor(private alertController: AlertController, public dbService: DatabaseService, private router: Router, private activatedRoute : ActivatedRoute,public modalController: ModalController) { }

  ngOnInit() {
    if(this.activatedRoute.snapshot.paramMap.get("id") != null)
    this.idDay = this.activatedRoute.snapshot.paramMap.get("id");
    
    this.dbService.getDias().subscribe( res => {
      //Guardo los datos de la bd en un array.
      this.data = res;

      //Formateo los ejercicios y me los guardo como array.
      let aux: any;
      aux = {
        ejercicios: this.data[this.idDay].ejercicios,
        img: this.data[this.idDay].img
      };

      let cont = 0;
      //Guardo en el array de ejercicios
      aux.ejercicios.forEach(item => {
        let aux2: Ejercicio;
        aux2 = {
          nombre: item,
          inputPeso: null,
          inputRepes: '',
          img: '<img class="iconoEjercicio" src='+aux.img[cont]+'>'
        }
        this.ejercicios.push(aux2);
        cont++;
      });

      if(this.idDay == 0){
        this.diaSem = 'Lunes';
      }else if(this.idDay == 1){
        this.diaSem = 'Martes';
      }else if(this.idDay == 2){
        this.diaSem = 'Jueves';
      }else if(this.idDay == 3){
        this.diaSem = 'Viernes';
      }

    });

    this.date = new Date();
    var dd = String(this.date.getDate()).padStart(2, '0');
    var mm = String(this.date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = this.date.getFullYear();

    this.fecha = yyyy + "-" + mm + "-" + dd;
  }

  closeWork(){
    this.presentAlertSub("Cerrar", "¿Estas seguro?", "No se guardaran los cambios");
  }

  async presentAlert(header, message) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Vale','Cancelar']
    });

    await alert.present();
  }

  async presentAlertSub(header, subHeader, message) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Vale',
          handler: () => {
            if(this.save){
              this.saveData();
              this.presentAlert("Entrenamiento completado", "<img src='../../assets/icon/tiburon.gif' style='height: 10px;width: 10px'>")
              setTimeout(() => this.router.navigate(['/home']), 1000);
              this.save = false;
            }else{
              this.router.navigateByUrl('/home');
            }
          }
        }
      ]
    });

    await alert.present();
    return true;
  }

  public trainingCompleted(){  
    this.presentAlertSub("Terminar", "¿Deseas acabar tu entrenamiento?", "");
    this.save = true;
  }

  //Guardado de datos en la bd
  public saveData(){
    let sessiones:Sessiones;
    let ejercicios: string[] = [];
    let repes: string[] = []
    let peso: number[] = [];

    this.ejercicios.forEach(item => {
      ejercicios.push(item.nombre),
      repes.push(item.inputRepes),
      peso.push(item.inputPeso)
    })  

    sessiones = {
      dia: this.weekday[this.date.getDay()],
      date: this.fecha,
      ejercicios: ejercicios,
      peso: peso,
      repes: repes,
    }

    this.dbService.saveSession(sessiones, this.fecha);
  }

}
