export interface Ejercicio{
    nombre: string;
    inputPeso: number,
    inputRepes: string,
    img: string;
}