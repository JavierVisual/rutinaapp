import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoutineDayPageRoutingModule } from './routine-day-routing.module';

import { RoutineDayPage } from './routine-day.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoutineDayPageRoutingModule
  ],
  declarations: [RoutineDayPage]
})
export class RoutineDayPageModule {}
