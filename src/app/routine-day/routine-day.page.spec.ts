import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoutineDayPage } from './routine-day.page';

describe('RoutineDayPage', () => {
  let component: RoutineDayPage;
  let fixture: ComponentFixture<RoutineDayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutineDayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoutineDayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
