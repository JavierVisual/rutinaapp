import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutineDayPage } from './routine-day.page';

const routes: Routes = [
  {
    path: '',
    component: RoutineDayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutineDayPageRoutingModule {}
