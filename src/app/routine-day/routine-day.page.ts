import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Dia } from "../model/dia.module";
import { DatabaseService } from "../services/database.service";

@Component({
  selector: 'app-routine-day',
  templateUrl: './routine-day.page.html',
  styleUrls: ['./routine-day.page.scss'],
})
export class RoutineDayPage{

  rutina: Dia[] = []
  data: any[] = [];
  idDay;
  diaSem: string;
  objectUser;
  objetivo;

  constructor(public alertController: AlertController, public dbService: DatabaseService, private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
    this.idDay = this.activatedRoute.snapshot.paramMap.get("id");

    if(this.idDay == 0){
      this.diaSem = 'Lunes';
    }else if(this.idDay == 1){
      this.diaSem = 'Martes';
    }else if(this.idDay == 2){
      this.diaSem = 'Jueves';
    }else if(this.idDay == 3){
      this.diaSem = 'Viernes';
    }
    
    this.dbService.getDias().subscribe( res => {
      //Guardo los datos de la bd en un array.
      this.data = res;
      
      //Formateo los datos y los guardo como dia.
      let aux: Dia;
      aux = {
        ejercicios: this.data[this.idDay].ejercicios,
        repes: this.data[this.idDay].repes,
        img: this.data[this.idDay].img
      };

      //Guardo los datos formateados en el array dia
      let cont = 0;
      let aux2: any;
      aux.ejercicios.forEach(item => {
        aux2 = {
          ejercicio: item,
          repes: aux.repes[cont],
          img: aux.img[cont]
        }
        this.rutina.push(aux2);
        cont++;
      })
    });

    this.dbService.getPerfil("javier").subscribe( res => {
      this.objectUser = res;
      this.objetivo = this.objectUser.objetivo;
    });
  }
}
