import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'routine-day/:id',
    loadChildren: () => import('./routine-day/routine-day.module').then( m => m.RoutineDayPageModule)
  },
  {
    path: 'work/:id',
    loadChildren: () => import('./work/work.module').then( m => m.WorkPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./modulesHome/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'rutina',
    loadChildren: () => import('./modulesHome/rutina/rutina.module').then( m => m.RutinaPageModule)
  },
  {
    path: 'sesiones',
    loadChildren: () => import('./modulesHome/sesiones/sesiones.module').then( m => m.SesionesPageModule)
  },
  {
    path: 'sesion/:id',
    loadChildren: () => import('./modulesHome/sesiones/sesion/sesion.module').then( m => m.SesionPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
