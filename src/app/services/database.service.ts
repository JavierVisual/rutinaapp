import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
import { map } from 'rxjs/operators'
import { Dia } from "../model/dia.module";
import { Sessiones } from "../model/sessiones.module";
import { User } from "../model/user.module";

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private db: AngularFirestore) { }

  getDias(){
    return this.db.collection('dias').snapshotChanges().pipe(map(dias =>{
      return dias.map(a =>{
        const data = a.payload.doc.data() as Dia;
        return data;
      })
    }));
  }

  getPerfiles(){
    return this.db.collection('perfiles').snapshotChanges().pipe(map(dias =>{
      return dias.map(a =>{
        const data = a.payload.doc.data() as Dia;
        return data;
      })
    }));
  }

  getSesions(){
    return this.db.collection('sesiones').snapshotChanges().pipe(map(sesiones =>{
      return sesiones.map(a =>{
        const data = a.payload.doc.data() as Sessiones;
        return data;
      })
    }));
  }

  getPerfil(perfil_id:string){
    return this.db.collection('perfiles').doc(perfil_id).valueChanges()
  }

  getDia(dia_id:string){
    return this.db.collection('dias').doc(dia_id).valueChanges()
  }

  getSession(session_id:string){
    return this.db.collection('sesiones').doc(session_id).valueChanges()
  }

  saveSession(sessiones:Sessiones, session_id:string){
    this.db.collection('sesiones').doc(session_id).set({
      dia: sessiones.dia,
      date: sessiones.date,
      ejercicios: sessiones.ejercicios,
      peso: sessiones.peso,
      repeticiones: sessiones.repes
    })
  }

  savePerfil(user:User, user_id){
    console.log(user)
    this.db.collection('perfiles').doc(user_id).update({
      nombre : user.nombre,
      objetivo: user.objetivo,
      peso: user.peso
    })
  }

  deleteSesion(session_id:string){
    return this.db.collection("sesiones").doc(session_id).delete();
  }

  


}
